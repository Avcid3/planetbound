/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.util.*;
import logic.data.*;
import logic.states.*;
/**
 *
 * @author aleja
 */
public class PlanetBound {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic her
        
        iniciarPartida();
    }
    

    public static void iniciarPartida() {
        System.out.println("Bienvenido a Planet Bound, vas a comenzar una nueva partida");
        Scanner sc = new Scanner(System.in);
        System.out.println("Primero escoja el tipo de nave que prefiere. 1 para Militar o 2 para Minera: ");
        int op;
        do{
          op = sc.nextInt();
        }while( op < 1 || op > 2);
        
        
        ArmasYEscudo armas;  
        Cargamento carga;
        Combustible combustible;        
        NaveEspacial nave;
        
        switch(op){
            case 1:
                armas = new ArmasYEscudo(9,9,18);
                carga = new Cargamento("Militar");
                combustible = new Combustible(7);
                
                nave = new NaveMilitar("Militar",armas,carga,combustible);
                break;
                
            case 2:
                armas = new ArmasYEscudo(9,9);
                carga = new Cargamento("Minera");
                combustible = new Combustible(53);
                
                nave = new NaveMinera("Minera",armas,carga,combustible);
                break;
                
            default:
                armas = new ArmasYEscudo(9,9);
                carga = new Cargamento("Militar");
                combustible = new Combustible(53);
                
                nave = new NaveMinera("Militar",armas,carga,combustible);
                break;
        }
        
        System.out.println("Comencemos el viaje");
        
        int turno = 1;

        //Partida
        while(nave.getArtefactosDescubiertos()!=5 && nave.getCombustible().getCombustible()>0 && !nave.getTripulacion().isEmpty()){
          double x = Math.random();
          Planeta planeta;
             if( x < 0.7){
               planeta =  new PlanetaBlanco(nave, false);
             }else{
               planeta =  new PlanetaRojo(nave, true);
             }
             
            System.out.println("Hemos encontrado un planeta. Vamos a dirigirnos hacia él");
            
            x = Math.random(); 
            if( x < 0.87){
                System.out.println("Avanzamos"); 
                MovimientoNormal m = new MovimientoNormal(nave);
                nave = m.moverse();
            }else{
                System.out.println("¡¡¡Un agujero negro!!!. Preparados para atravesarlo"); 
                AgujeroNegro a = new AgujeroNegro(nave);
                nave = a.pasarAgujero();
            }
            
            if(!comprobarNave(nave)){
               break;
            }
            
        }
        
        finalizarPartida(nave);
        
    }

    private static boolean comprobarNave(NaveEspacial nave) {
        if(nave.getCombustible().getCombustible()==0){
            return false;
        }else{
           if(nave.getTripulacion().isEmpty()){
              return false; 
           }
        }
      return true;  
    }

    private static void finalizarPartida(NaveEspacial nave) {
        if(nave.getCombustible().getCombustible()==0){
            System.out.println("¡¡¡Maldita sea!!! Nos hemos quedado sin combustible, hasta aquí llega nuestro viaje camarada");
        }else{
           if(nave.getTripulacion().isEmpty()){
               System.out.println("¡¡¡Diantres!!! Hemos perdido a toda la tripulación, lo siento pero no podemos continuar viajando"); 
           }else{
               System.out.println("¡¡¡Enhorabuena!!! Has encontrado todos los artefactos.\n Es hora de regresar a casa y celebrar tu victoria");
           }
        } 
        
        System.out.println("¿Deseas jugar de nuevo? \n 1 para Sí y 2 para No");
        Scanner sc = new Scanner(System.in);
        int op;
        do{
          op = sc.nextInt();
        }while( op < 1 || op > 2);
        if(op==1){
         System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
         iniciarPartida(); 
        }else{
            System.out.println("Muchas gracias por haber jugado. Nos vemos en otra ocasión");  
        }
    }

    
}
