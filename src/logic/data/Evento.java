/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

/**
 *
 * @author aleja
 */
public class Evento {
    NaveEspacial nave;
    
    public Evento(NaveEspacial nave){
        this.nave=nave;
        realizarEvento();
    }
    
    public Evento(NaveEspacial nave,int evento){
        this.nave=nave;
        realizarEvento(evento);
    }

    public NaveEspacial getNave() {
        return nave;
    }
    
    private void realizarEvento(int op){
        switch(op){
            case 1:
                muerteTripulacion();
                break;
            case 2:
                naveAbandonada();
                break;
            case 3:
                perdidaCarga();
                break;
            case 4:
                perdidaCombustible();
                break; 
            case 5:
                sinEvento();
                break;
            case 6:
                rescateTripulacion();
                break;
        }
    }
    
    private void realizarEvento(){
        int op = (int)(Math.random()*6+1);
        switch(op){
            case 1:
                muerteTripulacion();
                break;
            case 2:
                naveAbandonada();
                break;
            case 3:
                perdidaCarga();
                break;
            case 4:
                perdidaCombustible();
                break; 
            case 5:
                sinEvento();
                break;
            case 6:
                rescateTripulacion();
                break;
        }
    }
    
    public void muerteTripulacion(){
       if(nave.getTripulacion().size()==1){      
           System.out.println("Vaya, parece que te has quedado sin tripulación, fin del viaje.");
           nave.eliminarTripulacion(0);
       }else{
           int x = (int)(Math.random()*(nave.getTripulacion().size()-1) +1);
           System.out.println("Lo siento, un miembro de tu tripulación ha muerto");
           nave.eliminarTripulacion(x);
       }
    }
   
    public void naveAbandonada(){
        System.out.println("¡¡¡Que bien!!!, hemos encontrado una nave abandonada, a ver que podemos extraer de ella");
        Cargamento c = nave.getCarga();  
        c.cargaAleatoria();
        nave.setCarga(c);
    }
    
    public void perdidaCarga(){
        System.out.println("Oh no, se ha perdido parte de la carga");
        Cargamento c = nave.getCarga();
        c.perdidaCargamento();
        nave.setCarga(c);
    }
    
    public void perdidaCombustible(){
        System.out.println("Ha ocurrido un accidente, vamos a perder combustible");
        Combustible c = nave.getCombustible();
        c.perderCombustible();
        nave.setCombustible(c);
        
    }
    
    public void sinEvento(){ 
        System.out.println("Eres afortunado, no se ha detectado ninguna incidencia");
    }
    
    public void rescateTripulacion(){
        System.out.println("Oh, hemos encontrado una nave abandonada. ¡¡¡Parece que hay alguien dentro!!!");
        if(nave.getTripulacion().size()== 6){
            System.out.println("Pero desgraciadamente no tenemos suficiente espacio para poder rescatarlo.");
        }else{
            System.out.println("Vamos a rescatarlo");
            nave.añadirTripulacion();
        }
    }
    
    public NaveEspacial finalizarEvento(){
        System.out.println("Evento finalizado. Gasta uno de combustible y continua la aventura");
        Combustible c = nave.getCombustible();
        c.perderCombustible();
        nave.setCombustible(c);
        return this.nave;
    }
}
