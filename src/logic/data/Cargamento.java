/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

import java.util.*;

/**
 *
 * @author aleja
 */
public class Cargamento {
  ArrayList<int[]> niveles;
  int nivelActual;
  int totalNegro;
  int totalRojo;
  int totalAzul;
  int totalVerde;
  int capacidadMaxima;
  
    public Cargamento(String tipoNave) {
       this.niveles = new ArrayList();
       rellenarNiveles(tipoNave);
       this.nivelActual=1;
       this.totalNegro=0;
       this.totalRojo=0;
       this.totalAzul=0;
       this.totalVerde=0;
       this.capacidadMaxima=this.nivelActual*6;
    }
    
 private void rellenarNiveles(String tipo){
     System.out.println(tipo);
     if(tipo.equals("Militar")){
         for (int i = 0; i < 2; i++) {
            niveles.add(rellenarCargamento());
         }
     }else{
         for (int i = 0; i < 4; i++) {
            niveles.add(rellenarCargamento());
         }
     }
 }
 
private int[] rellenarCargamento(){
 int[] aux = new int[4];
         for (int i = 0; i < aux.length; i++) {
            aux[i] = 0;
         }
    return aux;
}
 
public int[] devolverNivel(int nivel){
    return niveles.get(nivel);
}

public int getNivelActual() {
    return nivelActual;
}

public int getTotalNegro() {
    return totalNegro;
}

public void setTotalNegro(int totalNegro) {
    this.totalNegro = totalNegro;
}

public int getTotalRojo() {
    return totalRojo;
}

private void setNivelActual(int niv) {
    this.nivelActual= niv;
}
 
public void setTotalRojo(int totalRojo) {
    this.totalRojo = totalRojo;
}

public int getTotalAzul() {
    return totalAzul;
}

public void setTotalAzul(int totalAzul) {
    this.totalAzul = totalAzul;
}

public int getTotalVerde() {
    return totalVerde;
}

public void setTotalVerde(int totalVerde) {
    this.totalVerde = totalVerde;
}

public int getCapacidadMaxima() {
    return capacidadMaxima;
}

public void setCapacidadMaxima(int capacidadMaxima) {
    this.capacidadMaxima = capacidadMaxima;
}

public void setCarga( int nivel, int [] carga){
    niveles.set(nivel, carga);
}

public void cargaAleatoria(){
    int tipoCarga = (int)(Math.random()*4);
    int valorDado = (int)(Math.random()*6+1);
    if(tipoCarga==0){
        System.out.println("Vas a añadir recursos negros");
        if((getTotalNegro() + valorDado)<= getCapacidadMaxima()){
            setTotalNegro(getTotalNegro() + valorDado);
        }else{
            setTotalNegro(getCapacidadMaxima());
        }
    }
    if(tipoCarga==1){
        System.out.println("Vas a añadir recursos rojos");
        if((getTotalRojo() + valorDado)<= getCapacidadMaxima()){
            setTotalRojo(getTotalRojo() + valorDado);
        }else{
            setTotalRojo(getCapacidadMaxima());
        }
    }
    if(tipoCarga==2){
        System.out.println("Vas a añadir recursos azules");
        if((getTotalAzul() + valorDado)<= getCapacidadMaxima()){
            setTotalAzul(getTotalAzul() + valorDado);
        }else{
            setTotalAzul(getCapacidadMaxima());
        }
    }
    if(tipoCarga==3){
        System.out.println("Vas a añadir recursos verdes");
        if((getTotalVerde() + valorDado)<= getCapacidadMaxima()){
            setTotalVerde(getTotalVerde() + valorDado);
        }else{
            setTotalVerde(getCapacidadMaxima());
        }
    }
    
  actualizarCargamento();     
}
public void mejorarCargamento(){
    
    setNivelActual(getNivelActual()+1);
    setCapacidadMaxima(getNivelActual() * 6);
    
    System.out.println("Cargamento mejorado. Actualmente esta a nivel " + (getNivelActual() -1));
}

public void actualizarCargamento(){
   int nivelActualizado=0;
   int auxNegro = getTotalNegro() ;
   int auxRojo = getTotalRojo() ;
   int auxAzul = getTotalAzul() ;
   int auxVerde = getTotalVerde() ;
   
   while(nivelActualizado < getNivelActual()){
       
        int[] aux = new int[4];
        int cont=0;
        while(cont<6 && auxNegro>0){
            aux[0] = aux[0] + 1;
            auxNegro--;
            cont++;
        }
        cont=0;
        while(cont<6 && auxRojo>0){
            aux[1] = aux[1] + 1;
            auxRojo--;
            cont++;
        }
        cont=0;
        while(cont<6 && auxAzul>0){
            aux[2] = aux[2] + 1;
            auxAzul--;
            cont++;
        }
        cont=0;
        while(cont<6 && auxVerde>0){
            aux[3] = aux[3] + 1;
            auxVerde--;
            cont++;
        }
       setCarga(nivelActualizado,aux);
       
       nivelActualizado++;
   }
   
   System.out.println("Cargamento actualizado");
   mostrarCargamento();
}

public void perdidaCargamento(){
    int tipoCarga = (int)(Math.random()*4); //Negro rojo azul o verde en ese orden
    
    if(tipoCarga==0){
        System.out.println("Vas a perder un recurso negro");
        if(getTotalNegro()>0){
            setTotalNegro(getTotalNegro() -1);
        }
    }
    if(tipoCarga==1){
        System.out.println("Vas a perder un recurso rojo");
        if(getTotalRojo()>0){
            setTotalRojo(getTotalRojo() -1);
        }
    }
    if(tipoCarga==2){
        System.out.println("Vas a perder un recurso azul");
        if(getTotalAzul()>0){
            setTotalAzul(getTotalAzul() -1);
        }
    }
    if(tipoCarga==3){
        System.out.println("Vas a perder un recurso verde");
        if(getTotalVerde()>0){
            setTotalVerde(getTotalVerde() -1);
        }
    }
    
    actualizarCargamento();   
} 

public void mostrarCargamento(){
    StringBuilder toret = new StringBuilder();
    toret.append("Nivel   Negro   Rojo   Azul   Verde\n");
    for (int i = niveles.size()-1; i >=0 ; i--) {
        toret.append("  ");
        toret.append(i);
        toret.append("  ");
        int[] aux = devolverNivel(i);
        for (int j = 0; j < aux.length; j++) {
           toret.append("---");
           toret.append("  ");
           toret.append(aux[j]);
           if(j==1 || j == 2){
           toret.append(" ");    
           }else{
           toret.append("  ");
           }
        } 
        toret.append("\n");
    }
    System.out.println(toret.toString());
}
}
