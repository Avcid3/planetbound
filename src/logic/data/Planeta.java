/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

/**
 *
 * @author aleja
 */
public abstract class Planeta {
    String tipo;
    int cantRecursos;
    int visitas;
    boolean artefacto;
    boolean estacion;
    Terreno t;
    NaveEspacial nave;

    public Planeta(NaveEspacial n, boolean estacion) {
        this.tipo = sacarTipo();
        this.artefacto=false;
        this.cantRecursos = cantRecursos();
        this. visitas = 0;
        this.estacion = estacion;
        this.t = new Terreno();
        this.nave = n;
    }
    
    private String sacarTipo(){
        int x = (int)(Math.random()*4);
        String t ="";
        if(x==0){
          t="Negro";  
        }
        
        if(x==1){
          t="Rojo";   
        }
        
        if(x==2){
          t="Azul";    
        }
        
        if(x==3){
          t="Verde";   
        }  
    return t;    
    }
   
    private int cantRecursos(){
        int x = 0;
        if(this.tipo.equals("Azul")){
          x =  4;
          this.artefacto = true;
        }else{
          x = 2;  
        }
        return x;
    }
    
}
