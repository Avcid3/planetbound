/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

/**
 *
 * @author aleja
 */
public class Combustible {
    int combustible;

    public Combustible(int combustible) {

        this.combustible = combustible;
    }

    public int getCombustible() {
        return combustible;
    }

    public void setCombustible(int combustible) {
        this.combustible = combustible;
    }

    public void perderCombustible() {
        setCombustible(getCombustible()-1);
        System.out.println("Ahora tienes " + getCombustible() + " de combustible");
    }
    public void perderCombustible(int cant) {
        if(getCombustible()>=cant){
        setCombustible(getCombustible()- cant);
        }else{
        setCombustible(0);    
        }
        System.out.println("Ahora tienes " + getCombustible() + " de combustible");
    }
   public void añadirCombustible() {
        setCombustible(getCombustible()-1);
        System.out.println("Ahora tienes " + getCombustible() + " de combustible");
    }  
}
