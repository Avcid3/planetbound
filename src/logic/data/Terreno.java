/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

import java.util.*;

/**
 *
 * @author aleja
 */
public class Terreno {
    String[][] superficie;
    int[] posNave;
    int[] posRecurso;
    int[] posAlien;
    
    public Terreno(){
        superficie= new String[6][6];
        posNave= new int[2];
        posRecurso= new int[2];
        posAlien= new int[2];
        rellenarTerreno();
        posicionarElementos();
    }
    
    private void posicionarElementos(){
        int fila;
        int columna;
        
        fila = (int)(Math.random()*6);
        posRecurso[0]=fila;
        columna = (int)(Math.random()*6);
        posRecurso[1]=columna;

        do{
            fila = (int)(Math.random()*6);
            columna = (int)(Math.random()*6);   
        }while(posRecurso[0]==fila && posRecurso[1]==columna);
        posNave[0]=fila;
        posNave[1]=columna;
        
        do{
           fila = (int)(Math.random()*6);
           columna = (int)(Math.random()*6);   
        }while((posRecurso[0]==fila && posRecurso[1]==columna) || (posNave[0]==fila && posNave[1]==columna));
        posAlien[0]=fila;
        posAlien[1]=columna; 
        
        superficie[posRecurso[0]][posRecurso[1]]="Recurso";
        superficie[posNave[0]][posNave[1]]="Nave";
        superficie[posAlien[0]][posAlien[1]]="Alien";
    }
    private void rellenarTerreno(){
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                superficie[i][j]="Terreno";
            }
        }
    }
    
    public void mostrarTerreno(){
    StringBuilder toret = new StringBuilder();
        for (int i = 0; i <67; i++) {
             toret.append("-");
        }
        toret.append("\n");
        for (int i = 0; i < 6; i++) {

            for (int j = 0; j < 6; j++) {
                 toret.append("|  ");
                 if(superficie[i][j].equals("Alien")){
                   toret.append(" "); 
                   toret.append(superficie[i][j]);
                   toret.append(" ");
                 }else{
                     if(superficie[i][j].equals("Nave")){
                         toret.append("  "); 
                         toret.append(superficie[i][j]);
                         toret.append(" ");  
                     }else{
                            toret.append(superficie[i][j]);
                           
                     } 
                 } 
                 toret.append(" ");  
            }
            toret.append("| \n");
            for (int x = 0; x <67; x++) {
             toret.append("-");
            }
            toret.append("\n");
        }

        System.out.println(toret.toString());
    }
}
