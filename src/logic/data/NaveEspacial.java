/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

import java.util.*;

/**
 *
 * @author aleja
 */
public abstract class NaveEspacial {
    private String tipoNave;
    private ArrayList<String>  tripulacion;
    private int artefactosDescubiertos;
    private ArmasYEscudo armas;
    private Cargamento carga;
    private Combustible combustible;

    public NaveEspacial(String tipoNave, ArmasYEscudo armas, Cargamento carga, Combustible combustible) {
        this.tipoNave = tipoNave;
        this.tripulacion= new ArrayList();
        this.artefactosDescubiertos=0;
        this.armas = armas;
        this.carga = carga;
        this.combustible = combustible;
        rellenarTripulacion();
    }

    public String getTipoNave() {
        return tipoNave;
    }

    public ArmasYEscudo getArmas() {
        return armas;
    }

    public void setArmas(ArmasYEscudo armas) {
        this.armas = armas;
    }


    public Cargamento getCarga() {
        return carga;
    }

    public void setCarga(Cargamento carga) {
        this.carga = carga;
    }

    public Combustible getCombustible() {
        return combustible;
    }

    public void setCombustible(Combustible combustible) {
        this.combustible = combustible;
    }

    public int getArtefactosDescubiertos() {
        return artefactosDescubiertos;
    }
    
    public void añadirArtefacto() {
       this.artefactosDescubiertos++;
    }
    
    public ArrayList<String> getTripulacion() {
        return tripulacion;
    }

    public boolean contieneTripulacion(String trip) {
        if(tripulacion.contains(trip)){
            return true;
        }else{
            return false;
        }
    }
    
    public void añadirTripulacion() {
        ArrayList<String> tripFalta = new ArrayList();
        if(!tripulacion.contains("Navegacion")){
            tripFalta.add("Navegacion");
        }
        if(!tripulacion.contains("Armas")){
            tripFalta.add("Armas");
        }
        if(!tripulacion.contains("Escudos")){
            tripFalta.add("Escudos");
        }
        if(!tripulacion.contains("Cargamento")){
            tripFalta.add("Cargamento");
        }
        if(!tripulacion.contains("Exploracion")){
            tripFalta.add("Exploracion");
        }
        int x = (int)(Math.random()*tripFalta.size());
        this.tripulacion.add(tripFalta.get(x));
        System.out.println("Bienvenido!!, eres el nuevo oficial de " + tripFalta.get(x));
    }
    
    public void añadirTripulacionEstacion() {
        ArrayList<String> tripFalta = new ArrayList();

        if(!tripulacion.contains("Navegacion")){
            tripFalta.add("Navegacion");
        }
        if(!tripulacion.contains("Armas")){
            tripFalta.add("Armas");
        }
        if(!tripulacion.contains("Escudos")){
            tripFalta.add("Escudos");
        }
        if(!tripulacion.contains("Cargamento")){
            tripFalta.add("Cargamento");
        }
        if(!tripulacion.contains("Exploracion")){
            tripFalta.add("Exploracion");
        }
        
        
        if(tripFalta.size()==1){
            tripulacion.add(tripFalta.get(0));
            System.out.println("Bienvenido!!, eres el nuevo oficial de " + tripFalta.get(0));  
        }else{
            System.out.println("Escoja un tipo de oficial");
            for (int i = 0; i < tripFalta.size(); i++) {
                System.out.println((i+1) + " para oficial de " + tripFalta.get(i));
            }
            int op; 
            do{
            Scanner sc = new Scanner(System.in);
            op = sc.nextInt();
            }while(op<1 || op>tripFalta.size());
            tripulacion.add(tripFalta.get(op));
            System.out.println("Bienvenido!!, eres el nuevo oficial de " + tripFalta.get(op));   
        }
              
    }
    
            
    public void eliminarTripulacion(int pos) {
        System.out.println("El oficial de " + this.tripulacion.get(pos) + " ha fallecido");
        this.tripulacion.remove(pos);

    }

    private void rellenarTripulacion() {
        tripulacion.add("Capitan");
        tripulacion.add("Navegacion");
        tripulacion.add("Armas");
        tripulacion.add("Escudos");
        tripulacion.add("Cargamento");
        tripulacion.add("Exploracion");
    }
}
