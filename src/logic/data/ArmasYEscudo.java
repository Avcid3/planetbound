/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.data;

/**
 *
 * @author aleja
 */
public class ArmasYEscudo {
    String[] escudos;
    String[] armas1;
    String[] armas2;
    int posEscudosActual;
    int posArmas1Actual;
    int posArmas2Actual;

    public ArmasYEscudo(int escudos, int armas1) {
        this.escudos =  new String[escudos];
        this.armas1 = new String[armas1];
        rellenarEscudos();
        rellenarArmas1();
        this.posEscudosActual = escudos;
        this.posArmas1Actual = armas1;
    }

    public ArmasYEscudo(int escudos, int armas1, int armas2) {
        this.escudos = new String[escudos];
        this.armas1 = new String[armas1];
        this.armas2 = new String[armas2];
        rellenarEscudos();
        rellenarArmas1();
        rellenarArmas2();
        this.posEscudosActual = escudos;
        this.posArmas1Actual = armas1;
        this.posArmas2Actual = 0;
    }

private void rellenarEscudos() {
    for (int i = 0; i < escudos.length; i++) {
        escudos[i]= ""+(i+1);
    }
} 

private void rellenarArmas1() {
    for (int i = 0; i < armas1.length; i++) {
        armas1[i]=""+(i+1);
    }
}

private void rellenarArmas2() {
    for (int i = 0; i < armas2.length; i++) {
        armas2[i]=""+(i+1);
    }
}

public int getPosEscudosActual() {
    return posEscudosActual;
}

public void setPosEscudosActual(int posEscudosActual) {
    this.posEscudosActual = posEscudosActual;
}

public int getPosArmas1Actual() {
    return posArmas1Actual;
}

public void setPosArmas1Actual(int posArmas1Actual) {
    this.posArmas1Actual = posArmas1Actual;
}

public int getPosArmas2Actual() {
    return posArmas2Actual;
}

public void setPosArmas2Actual(int posArmas2Actual) {
    this.posArmas2Actual = posArmas2Actual;
}

public void añadirEscudo(){
    
}

public void perderEscudo(int cant){
    for (int i = 0; i < cant; i++) {
      if(getPosEscudosActual()>0){
         escudos[getPosEscudosActual()-1]="Vacío";
         setPosEscudosActual(getPosEscudosActual()-1);
      } 
    }
    System.out.println("Quedan " + getPosEscudosActual() + " escudos");
}

}
