/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.states;

import logic.data.*;

/**
 *
 * @author aleja
 */
public class AgujeroNegro {
    NaveEspacial nave;

    public AgujeroNegro(NaveEspacial nave) {
        this.nave = nave;
    }

    public NaveEspacial getNave() {
        return nave;
    }

    public void setNave(NaveEspacial nave) {
        this.nave = nave;
    }
    
    
    public NaveEspacial pasarAgujero(){
        Combustible c = nave.getCombustible();
        ArmasYEscudo a = nave.getArmas(); 
        if(nave.contieneTripulacion("Escudos")){
            if(a.getPosEscudosActual()>=2){
                c.perderCombustible(3);
                a.perderEscudo(2); 
            }else{
                c.perderCombustible(3);
                a.perderEscudo(2); 
                if(nave.getTripulacion().size()==1){      
                    System.out.println("Vaya, parece que te has quedado sin tripulación, fin del viaje.");
                    nave.eliminarTripulacion(0);
                }else{
                    int x = (int)(Math.random()*(nave.getTripulacion().size()-1) +1);
                    System.out.println("Lo siento, un miembro de tu tripulación ha muerto");
                    nave.eliminarTripulacion(x);
                }
            }
        }else{
            if(a.getPosEscudosActual()>=4){
                c.perderCombustible(4);
                a.perderEscudo(4); 
            }else{
                c.perderCombustible(4);
                a.perderEscudo(4);
                if(nave.getTripulacion().size()==1){      
                    System.out.println("Vaya, parece que te has quedado sin tripulación, fin del viaje.");
                    nave.eliminarTripulacion(0);
                }else{
                    int x = (int)(Math.random()*(nave.getTripulacion().size()-1) +1);
                    System.out.println("Lo siento, un miembro de tu tripulación ha muerto");
                    nave.eliminarTripulacion(x);
                }  
            }
        }
        nave.setArmas(a);
        nave.setCombustible(c);
        System.out.println("Ha sido duro, pero hemos conseguido pasar.");
        return getNave();
    }
}
