/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic.states;

import java.util.*;
import logic.data.*;


/**
 *
 * @author aleja
 */
public class MovimientoNormal {
   NaveEspacial nave;

    public MovimientoNormal(NaveEspacial nave) {
        this.nave = nave;
    }

    public NaveEspacial getNave() {
        return nave;
    }

    public void setNave(NaveEspacial nave) {
        this.nave = nave;
    }
    
    public NaveEspacial moverse(){
      System.out.println("Nuevo evento detectado. Quieres que sea aleatorio (1) o manual (2)? ");
         int n;
         Scanner sc = new Scanner(System.in);       
          do{
            n = sc.nextInt();
          }while( n < 1 || n > 2);
                
         if(n==1){
         Evento e = new Evento(getNave());
         setNave(e.finalizarEvento());
        }else{
           System.out.println("Los eventos a escoger son: \n 1) Muerte de un tripulante"
                            + "\n 2) Nave abandonada \n 3) Pérdida de cargamento \n 4) Pérdida de combustible"
                            + "\n 5) Sin evento \n 6) Rescate de un tripulante");
         do{
          n = sc.nextInt();
         }while( n < 1 || n > 6);
               Evento e = new Evento(getNave(),n);
               setNave(e.finalizarEvento());
       }
         return getNave();
    }
}
